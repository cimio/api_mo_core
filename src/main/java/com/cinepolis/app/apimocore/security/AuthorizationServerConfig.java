package com.cinepolis.app.apimocore.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    /**
     * Configure the security of the Authorization Server, which means in practical terms the /oauth/token endpoint
     *
     * @param security The security element
     * @throws Exception The exception
     * @Author amorenoc@cinepolis.com
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated");
    }

    /**
     * Information about OAuth2 clients and where their info is located: memory, DB, LDAP, etc
     * In this configuration I am using 12 hours of token validity in both cases
     * Token Validity and Refresh Token
     *
     * @param clients The clientDetailsServiceConfigurer
     * @throws Exception The exception
     * @Author amorenoc@cinepolis.com
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().withClient("appName")
                .secret(bCryptPasswordEncoder.encode("123456"))
                .scopes("read", "write")
                .authorizedGrantTypes("password", "refresh_token")
                .accessTokenValiditySeconds(43200)
                .refreshTokenValiditySeconds(43200);
    }

    /**
     * Non-security configs for end points associated with the Authorization Server
     * and can specify details about authentication manager, token generation, etc
     *
     * @param endpoints The endpoints configurer
     * @throws Exception The exception
     * @Author amorenoc@cinepolis.com
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager)
                .accessTokenConverter(accessTokenConverter());
    }

    /**
     * Translates between JWT encoded token values and OAuth authentication information
     *
     * @return JwtAccessTokenConverter
     * @Author amorenoc@cinepolis.com
     */
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        return new JwtAccessTokenConverter();
    }
}
