package com.cinepolis.app.apimocore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMoCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiMoCoreApplication.class, args);
    }

}
