package com.cinepolis.app.apimocore.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/dumb")
public class DumbController {

    @GetMapping
    public String getDumb() {
        return "Dumb";
    }

    @PostMapping
    public String getDumb2() {
        return "Dumb";
    }

    @PutMapping
    public String getDumb3() {
        return "Dumb";
    }

    @DeleteMapping
    public void getDumb4() {

    }
}
